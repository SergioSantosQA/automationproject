package BDD.ToDoAppTesting;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		format = {"pretty", "html:target/cucumber", "json:json_report/cucumber.json", 
				"junit:junit_report/cucumber.xml"}, 
				monochrome = true,
		features = "src/tests/resource")
public class ToDoAppTestRunner {

}