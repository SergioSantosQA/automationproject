package BDD.ToDoAppTesting;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObjects.LoginPage;
import PageObjects.MainPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**Class to implement the steps definition for the features
* @author Sergio Santos
* @version 1.01
*/
public class ToDoAppTestSteps {	
	
	WebDriver driver;		
	
	@Before
	public void Setup() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
	}
	
	@After
	public void FinishTest(){	
		
		MainPage page = new MainPage(this.driver);
		
		if (page.VerifyModalWindowsPresence())
			page.TypeEnterOnSubtaskDescription(Keys.ESCAPE);		
		
		if (page.AreThereTasks()) {
			page.RemoveAllTasks();
			
		}		
				
		driver.quit();
		
	}
	
	@Given("^have some tasks created$")
	public void have_some_tasks_created(){
		
		MainPage page = new MainPage(this.driver);
		page.CreateSomeTasks(3);
		
	}

	@Then("^There is a column SubTasks with buttom manage subtasks$")
	public void there_is_a_column_SubTasks_with_buttom_manage_subtasks(){
		
		MainPage page = new MainPage(this.driver);
		Assert.assertTrue(page.VerifyFirstManageSubtaskButton());
			
	}
	
	@Given("^^There is a task with '(\\d+)' subtasks$")
	public void there_is_a_task_with_subtasks(int subTasksToBeCreated){

		MainPage page = new MainPage(this.driver);
		page.CreateTaskWithSubtasks(subTasksToBeCreated);	
		
	}

	
	@Then("^Manage Subtask button is labeled with \"(.*?)\" for the selected task$")
	public void manage_Subtask_button_is_labeled_with_for_the_given_task(String expectedText){
	
		MainPage page = new MainPage(this.driver);
		Assert.assertEquals(page.getManageSubtasksUniqueTaskButtonText(), expectedText);
		
	}

	
	@Given("^Have a task created$")
	public void have_a_task_created(){
		
		MainPage page = new MainPage(this.driver);
		page.CreateSomeTasks(1);
		
	}

	
	@When("^Clicking on Manage Subtasks button$")
	public void clicking_on_Manage_Subtasks_button(){

		
		MainPage page = new MainPage(this.driver);
		page.ClickManageSubtasksUniqueTaskButton();
		
	}

	
	@Then("^A modal dialog must open to manage subtasks$")
	public void a_modal_dialog_must_open_to_manage_subtasks(){

		MainPage page = new MainPage(this.driver);		
		Assert.assertTrue(page.VerifyModalWindowsPresence());		
		
	}

	
	@Then("^It must have a read only field with task ID on Manage Subtasks modal window$")
	public void it_must_have_a_read_only_field_with_task_ID_on_Manage_Subtasks_modal_window(){
		
		MainPage page = new MainPage(this.driver);
		Assert.assertTrue(page.getTaskIdFromModalTitle() != "");	
	
	}
	
	
	@When("^Type a valid text in Subtask Description field$")
	public void type_a_valid_text_in_Subtask_Description_field(){

		MainPage page = new MainPage(this.driver);
		Random generator = new Random();
		String subTaskName = generator.toString();
		page.FillSubytaskDescription(subTaskName);
		
	}

	
	@When("^Type a valid text in Due Date field$")
	public void type_a_valid_text_in_Due_Date_field(){

		MainPage page = new MainPage(this.driver);
		page.FillDueDate("11/30/2016");
		
	}
	
	
	@When("^Enter key typed to created task$")
	public void enter_key_typed_task(){

		MainPage page = new MainPage(this.driver);
		page.TypeEnterOnTaskDescription(Keys.RETURN);	
		
	}
	
	@When("^Enter key typed to created subtask$")
	public void enter_key_typed_subtask(){


		MainPage page = new MainPage(this.driver);
		page.TypeEnterOnSubtaskDescription(Keys.RETURN);	
		
	}

	
	@Then("^A new subtask must be created successfuly$")
	public void a_new_subtask_must_be_created_successfuly(){

		MainPage page = new MainPage(this.driver);
		Assert.assertTrue(page.VerifyUniqueSubtaskPresence());
		
	}

	
	@When("Click Add subtask button$")
	public void click_subtask_button(){

		MainPage page = new MainPage(this.driver);
		page.ClickAddSubtaskButton();
		
	}

	
	@When("^Create 3 subtasks$")
	public void create_subtasks(){

		MainPage page = new MainPage(this.driver);
		page.CreateSomeSubtasks(3);
		
	}

	
	@Then("3 subtasks must be on the bottom part of the modal dialog$")
	public void subtasks_must_be_on_the_bottom_part_of_the_modal_dialog(){
	
		MainPage page = new MainPage(this.driver);
		
		Assert.assertTrue(page.AreThereThreeSubtasks());			

	}

	
	@Then("^No subtasks must be created$")
	public void no_subtasks_must_be_created_and_a_validation_message_must_be_shown_for_the_field_Subtask_Description_required(){
			
		MainPage page = new MainPage(this.driver);
		Assert.assertFalse(page.VeryfyUniqueRemoveSubtaskButtonPresence());
		
	}

	
	@When("^Erase the Due Date field content$")
	public void erase_the_Due_Date_field_content(){

		MainPage page = new MainPage(this.driver);
		page.FillDueDate("");
		
	}

	
	@When("^Type more than 250 characters in subtask description field$")
	public void type_more_than_characters_in_subtask_description_field(){
		
		MainPage page = new MainPage(this.driver);
		
		page.FillSubytaskDescription("Subtask Name Subtask Name Subtask Name "
				+ "Subtask Name Subtask Name Subtask Name Subtask Name "
				+ "Subtask Name Subtask Name Subtask Name Subtask Name "
				+ "Subtask Name Subtask Name Subtask Name Subtask Name "
				+ "Subtask Name Subtask Name Subtask Name Subtask Name Subtask Name");
				
	}

	
	@When("^Type an invalid text in Due Date field$")
	public void type_an_invalid_text_in_Due_Date_field(){

		MainPage page = new MainPage(this.driver);
		page.FillDueDate("invalid date");
		
	}
		

	@Test
	@Given("^Open browser and start application$")
	public void open_browser_and_start_application(){
		
		driver.get("http://qa-test.avenuecode.com/users/sign_in");
		
	}

	
	@When("^Type an invalid user email and fill password field$")
	public void provide_an_invalid_user_email_and_fill_password_field(){

		LoginPage page = new LoginPage(this.driver);
		page.Login("invalidemail@email.com", "xxxxxxx");
		page.clickLogin();
		
	}

	
	@Then("^User should not be logged in$")
	public void user_should_not_be_logged_in(){

		LoginPage page = new LoginPage(this.driver);		
		String loginMessage = page.getLoginMessage();		
		Assert.assertEquals(loginMessage, "Invalid email or password.");		
		
	}

	
	@When("^Type valid user email and a wrong password$")
	public void provide_valid_user_email_and_a_wrong_password(){
		
		LoginPage page = new LoginPage(this.driver);
		page.Login("sergio20721220@gmail.com", "xxxxxxx");
		page.clickLogin();
		
		
	}

	
	@Then("^User should be logged in$")
	public void user_should_be_logged_in(){

		LoginPage page = new LoginPage(this.driver);		
		String loginMessage = page.getLoginMessage();
		Assert.assertEquals(loginMessage, "Signed in successfully.");
		
	}

	
	@When("^Provide valid user email and password$")
	public void provide_valid_user_email_and_password(){

		LoginPage page = new LoginPage(this.driver);
		page.Login("sergio20721220@gmail.com", "host100@");
		page.clickLogin();
		
	}

	@Test
	@Given("^Logged as ToDoApp user$")
	public void logged_as_ToDoApp_user(){
		
		this.provide_valid_user_email_and_password();
		//driver.findElement(By.tagName("form")).submit();

	}

	
	@Then("^My Tasks link is present$")
	public void my_Tasks_link_is_present(){

		MainPage page = new MainPage(this.driver);
		Assert.assertTrue(page.VerifyMyTasksPresent());
				
	}

	
	@When("^Clicking on My Tasks link$")
	public void clicking_on_My_Tasks_link(){

		MainPage page = new MainPage(this.driver);
		page.clickMyTaskLink();
		
	}

	
	@Then("^Tasks list is shown$")
	public void tasks_list_is_shown(){

		MainPage page = new MainPage(this.driver);
		Assert.assertTrue(page.VerifyTaskListPresent());
		
	}

	
	@Then("^Message for logged user must be Hey USER NAME user, this is your todo list for today:$")
	public void message_for_logged_user_must_be(){

		MainPage page = new MainPage(this.driver);
		String welcomeMessage = page.getWelcomeMessage();
		Assert.assertTrue(welcomeMessage == "Hey Logged User, this is your todo list for today:");  //complete

	}

	
	@When("^Type a valid text in task description field$")
	public void type_a_valid_text_in_task_description_field(){

		MainPage page = new MainPage(this.driver);
		page.FillTaskDescription("a valid description");
		
	}

	
	@Then("^A new task must be created$")
	public void a_new_task_must_be_created(){

		MainPage page = new MainPage(this.driver);
		Assert.assertTrue(page.AreThereTasks());

	}
	
	@When("^Click create task button$")
	public void Click_create_task_button(){
		
		MainPage page = new MainPage(this.driver);
		page.clickCreateTaskButton();
		
	}
	
	@When("^Click on task description input$")
	public void click_on_task_description_input(){

		MainPage page = new MainPage(this.driver);
		page.clickTaskDescription();
		
	}
	
	@When("^Type a text with less than 3 characters in task description field$")
	public void type_a_text_with_less_than_characters_in_task_description_field(){

		MainPage page = new MainPage(this.driver);
		page.FillTaskDescription("ta");
		
	}
	
	
	@When("^Type a text with more than 250 characters in task description field$")
	public void type_a_text_with_more_than_characters_in_task_description_field(){
		
		MainPage page = new MainPage(this.driver);
		
		page.FillTaskDescription("Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task Name "
				+ "Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task Name "
				+ "Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task Name T");
		
	}

	
	@Then("^No task must be created$")
	public void no_task_created_and_a_validation_message_must_be_shown_to_the_user_for_the_description_field_accepts_maximum_characters(){

		MainPage page = new MainPage(this.driver);		
		Assert.assertFalse(page.AreThereTasks());		
	
	}

	
	@When("^Type a text with exactly 250 characters in task description field$")
	public void type_a_text_with_exactly_characters_in_task_description_field(){

		MainPage page = new MainPage(this.driver);
		page.FillTaskDescription("Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task Name "
				+ "Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task Name "
				+ "Task Name Task Name Task Name Task Name Task Name Task Name Task Name Task NameT");
		
	}

	
	@Then("^A new task must be appended to task list$")
	public void a_new_task_must_be_appended_to_task_list(){

		MainPage page = new MainPage(this.driver);		
		Assert.assertTrue(page.AreThereTasks());	

	}
	
		
	}
