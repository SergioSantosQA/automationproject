package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**Class to implement the login page object. All elements and methods for the login page are found here.
* @author Sergio Santos
* @version 1.01
*/

public class LoginPage {
	
	@FindBy(how = How.ID, using = "user_email")
	public WebElement txtEmail;
	
	@FindBy(how = How.ID, using = "user_password")
	public WebElement txtPassword;
	
	@FindBy(tagName = "form")
	public WebElement frmLogin;
	
	@FindBy(xpath = "html/body/div[1]/div[2]")
	public WebElement loginMessage;
	
	
	public LoginPage(WebDriver driver){
		
		PageFactory.initElements(driver, this);
		
	}
	
	public void Login(String email, String password){
		
		txtEmail.sendKeys(email);
		txtPassword.sendKeys(password);
		
	}
	
	public void clickLogin(){
		
		frmLogin.submit();
		
	}
	
	public String getLoginMessage(){
		
		return loginMessage.getText().trim();
		
	}
	
	
	
	
	
}
