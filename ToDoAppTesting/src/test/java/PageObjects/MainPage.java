package PageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**Class to implement the main page object. All elements and methods for the main page are found here.
* @author Sergio Santos
* @version 1.01
*/

public class MainPage {
	
	@FindBy(xpath = "html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a")
	public WebElement myTasksLink;
	
	@FindBy(xpath = "html/body/div[1]/div[2]")
	public WebElement Tasklist;
	
	@FindBy(xpath = "html/body/div[1]/h1")
	public WebElement welcomeMessage;
	
	@FindBy(how = How.ID, using = "new_task")
	public WebElement txtTaskDescription;
	
	@FindBy(how = How.ID, using = "new_sub_task")
	public WebElement txtSubtaskDescription;
	
	@FindBy(how = How.ID, using = "add-subtask")
	public WebElement btnAddSubtask;
	
	@FindBy(how = How.ID, using = "dueDate")
	public WebElement txtDueDate;	
	
	@FindBy(xpath = "html/body/div[1]/div[2]/div[1]/form/div[2]/span")
	public WebElement createTaskButton;
	
	@FindBy(xpath = "html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[4]/button")
	public WebElement manageSubtasksUniqueTaskButton;
	
	@FindBy(xpath = "html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[4]/button")
	public WebElement manageSubtaskFirstButton;
	
	@FindBy(xpath = "html/body/div[4]/div/div/div[2]")
	public WebElement modalWindow;
	
	@FindBy(xpath = "html/body/div[4]/div/div/div[1]/h3")
	public WebElement modalTitle;
	
	@FindBy(xpath = "html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]")
	public WebElement uniqueSubtaskCreated;
	
	@FindBy(xpath = "html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr[3]/td[2]")
	public WebElement thirdSubtaskLine;
	
	@FindBy(xpath = "html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[3]/button")
	public WebElement uniqueRemoveSubtaskButton;
	
	@FindBy(xpath = "html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[5]/button")
	public WebElement removeTaskFirstButton;
	
	@FindBy(xpath = "html/body/div[1]/div[2]/div[2]/div/table/tbody/tr/td[5]/button")
	public WebElement removeTaskUniqueButton;
	
	
public MainPage(WebDriver driver){
		
		PageFactory.initElements(driver, this);
		
	}	
	
	
	public boolean VerifyMyTasksPresent(){
		
		return myTasksLink.isDisplayed();
		
	}
	
	public boolean VerifyTaskListPresent(){
		
		return Tasklist.isDisplayed();
		
	}
	
	public void clickMyTaskLink(){
		
		myTasksLink.click();
		
	}
	
	public void clickCreateTaskButton(){
		
		createTaskButton.click();
		
	}
	
	public String getWelcomeMessage(){
		
		return welcomeMessage.getText().trim();
		
	}
	
	public void FillTaskDescription(String description){
		
	txtTaskDescription.sendKeys(description);
		
	}
	
	public void FillSubytaskDescription(String description){
		
		txtSubtaskDescription.sendKeys(description);
			
		}
	
	public void TypeEnterOnTaskDescription(Keys key){
		
		txtTaskDescription.sendKeys(key);
			
		}
	
	public void TypeEnterOnSubtaskDescription(Keys key){
		
		txtSubtaskDescription.sendKeys(key);
			
		}
	
	public void FillDueDate(String dueDate){
		
		txtDueDate.sendKeys(dueDate);
		
	}
	
	public void clickTaskDescription(){
		
		txtTaskDescription.click();
			
		}
	
	public boolean VerifyNewTaskCreated(){		
		
		return false;	
		
		}
	
	
	public boolean AreThereTasks(){
		
		boolean areThereTasks;
		
		try {
			manageSubtaskFirstButton.isDisplayed();
			   areThereTasks = true;
			} catch (NoSuchElementException e) {
				
				try {
					  manageSubtasksUniqueTaskButton.isDisplayed();
					  areThereTasks = true;
					  
					} catch (NoSuchElementException ex) {
						
						areThereTasks = false;
						
					}
				
			}
		
		return areThereTasks;
		
	}
	
public boolean AreThereSubtasks(){
		
		boolean areThereSubtasks;
		
		try {
			   manageSubtasksUniqueTaskButton.isDisplayed();
			   areThereSubtasks = true;
			} catch (NoSuchElementException e) {
				areThereSubtasks = false;
			}
		
		return areThereSubtasks;
		
	}
	
	
	public void CreateSomeTasks(int tasksToBeCreated){
		
		myTasksLink.click();	
		
		for (int i = 0; i < tasksToBeCreated; i++){
			
			txtTaskDescription.sendKeys("Task " + Integer.toString(i));
			txtTaskDescription.sendKeys(Keys.RETURN);			
		}		
		
	}
	
	public void CreateSomeSubtasks(int subtasksToBeCreated){
		
		for (int i = 0; i < subtasksToBeCreated; i++){
			
			txtSubtaskDescription.sendKeys("SubTask " + Integer.toString(i));
			btnAddSubtask.click();
			
		}		
		
	}
	
public void CreateTaskWithSubtasks(int subtasksToBeCreated){
	
	myTasksLink.click();
	txtTaskDescription.sendKeys("Task 1");	
	txtTaskDescription.sendKeys(Keys.RETURN);
	
	manageSubtasksUniqueTaskButton.click();
	
	for (int i = 0; i < subtasksToBeCreated; i++){
		
		txtSubtaskDescription.sendKeys("SubTask " + Integer.toString(i));
		btnAddSubtask.click();
		
	}
	
	txtSubtaskDescription.click();		
	txtSubtaskDescription.sendKeys(Keys.ESCAPE);	
		
	}
	
	public boolean VerifyFirstManageSubtaskButton(){
		
		return manageSubtaskFirstButton.isDisplayed();
		
	}
	
	public String getManageSubtasksUniqueTaskButtonText(){
		
		String buttonLabel = manageSubtasksUniqueTaskButton.getText().trim();
		return buttonLabel;
		
	}
	
	public void ClickManageSubtasksUniqueTaskButton(){
		
		manageSubtasksUniqueTaskButton.click();
		
	}
	
	public boolean VerifyModalWindowsPresence(){
		
		boolean modalPresent;		
		try {
			  
			   txtSubtaskDescription.isDisplayed();
			   modalPresent = true;
			   
			} catch (NoSuchElementException e) {
				
				modalPresent = false;
				
			}
		
		return modalPresent;
		
	}
	
	public String getTaskIdFromModalTitle(){
		
		String taskID = modalTitle.getText();
		taskID = taskID.trim().replace("Editing Task","");		
		
		return taskID;
		
	}
	
	public boolean VerifyUniqueSubtaskPresence(){
		
		return uniqueSubtaskCreated.isDisplayed();
		
	}
	
	public void ClickAddSubtaskButton(){
		
		btnAddSubtask.click();
		
	}
	
	public boolean AreThereThreeSubtasks(){
		
		return thirdSubtaskLine.isDisplayed();
		
	}
	
	public boolean VeryfyUniqueRemoveSubtaskButtonPresence(){
		
		return uniqueRemoveSubtaskButton.isDisplayed();
		
	}
	
	public void RemoveAllTasks(){
		
		try{
			
			while (removeTaskFirstButton.isDisplayed())			
				removeTaskFirstButton.click();	
				
			removeTaskUniqueButton.click();	
			
		} catch (NoSuchElementException e) {
			
			
			
		}
		

	}
	


}
