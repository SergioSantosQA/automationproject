Feature: Create SubTask
	As a ToDo App user I should be able to create a subtask
	So I can break down my tasks in smaller pieces
	
	Acceptance Criteria:
	1. The user should see a button labeled as ‘Manage Subtasks’
	2. This button should have the number of subtasks created for that tasks
	3. Clicking this button opens up a modal dialog
		-> This popup should have a read only field with the task ID and the task description
		-> There should be a form so you can enter the SubTask Description (250	characters) and SubTask due date (MM/dd/yyyy format)
		-> The user should click on the add button to add a new Subtask
		-> The Task Description and Due Date are required fields
		-> Subtasks that were added should be appended on the bottom part of the modal dialog

	#Scenario 18
	Scenario: Verify manage subtask button
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And have some tasks created
	Then  There is a column SubTasks with buttom manage subtasks
	
	#Scenario 19
	Scenario: Verify number of subtasks of a task in Manage Substask button
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And There is a task with '3' subtasks	
	Then Manage Subtask button is labeled with "(3) Manage Subtasks" for the selected task
	
	#Scenario 20
	Scenario: Verify modal window on Manage Subtasks button click
	Given Open browser and start application
	And Provide valid user email and password	
	And Clicking on My Tasks link
	And Have a task created
	When Clicking on Manage Subtasks button
	Then A modal dialog must open to manage subtasks
	
	#Scenario 21
	Scenario: Verify read only field with the task ID on modal window
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	When Clicking on Manage Subtasks button
	Then It must have a read only field with task ID on Manage Subtasks modal window
	
	#Scenario 22
	Scenario: Create a new subtask Successfully hit Enter key
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created	
	And Clicking on Manage Subtasks button
	When Type a valid text in Subtask Description field
	And  Type a valid text in Due Date field
	And Enter key typed to created subtask
	Then A new subtask must be created successfuly
	
	#Scenario 23
	Scenario: Create a new subtask Successfully click "+Add" button
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type a valid text in Subtask Description field
	And  Type a valid text in Due Date field
	And Click Add subtask button
	Then A new subtask must be created successfuly
	
	#Scenario 24
	Scenario: Verify Subtasks list on the bottom part of the modal dialog
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Create 3 subtasks
	Then 3 subtasks must be on the bottom part of the modal dialog
	
	#Scenario 25
	Scenario: Create a new subtask with "Subtask Description" field empty by click on "+Add" button
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type a valid text in Due Date field
	And Click Add subtask button
	Then No subtasks must be created
	
	
	#Scenario 26
	Scenario: Create a new subtask with "Subtask Description" field empty by hit Enter key
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type a valid text in Due Date field
	And Enter key typed to created subtask
	Then No subtasks must be created
	
	
	#Scenario 27
	Scenario: Create a new subtask with "Due Date" field empty by click on "+Add" button
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type a valid text in Subtask Description field
	And Erase the Due Date field content
	And Click Add subtask button
	Then No subtasks must be created
	
	#Scenario 28
	Scenario: Create a new subtask with "Due Date" field empty by hit Enter key
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type a valid text in Subtask Description field
	And Erase the Due Date field content
	And Enter key typed to created subtask
	Then No subtasks must be created
	
	#Scenario 29
	Scenario: Create a new subtask by hit Enter key with subtask description field with more than 250 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type more than 250 characters in subtask description field
	And  Type a valid text in Due Date field
	And Enter key typed to created subtask
	Then  No subtasks must be created
		
	#Scenario 30
	Scenario: Create a new subtask by click "+Add" button with subtask description field with more than 250 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type more than 250 characters in subtask description field
	And  Type a valid text in Due Date field
	And Click Add subtask button
	Then  No subtasks must be created
	
	#Scenario 31
	Scenario: Verify validation in Due Date field in a wrong format
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	And Have a task created
	And Clicking on My Tasks link
	And Clicking on Manage Subtasks button
	When Type a valid text in Subtask Description field
	And  Type an invalid text in Due Date field
	And Click Add subtask button
	Then  No subtasks must be created	
	