Feature: Create Task
	As a ToDo App user I should be able to create a task
	So I can manage my tasks
	
	Acceptance Criteria:
	1. The user should always see the ‘My Tasks’ link on the NavBar
		-> Clicking this link will redirect the user to a page with all the created tasks so far
	2. The user should see a message on the top part saying that list belongs to the logged user:
		-> eg.: If the logged user name is John, then the displayed message should be "Hey John, this is your todo list for today:"
	3. The user should be able to enter a new task by hitting enter or clicking on the add task button.
	4. The task should require at least three characters so the user can enter it.
	5. The task can’t have more than 250 characters.
	6. When added, the task should be appended on the list of created tasks.

	#Scenario 1
	Scenario: Try to login with not existing user mail
	Given Open browser and start application
	And Type an invalid user email and fill password field
	Then User should not be logged in
	
	#Scenario 2
	Scenario: Try to login with wrong password
	Given Open browser and start application
	And Type valid user email and a wrong password
	Then User should not be logged in
	
	#Scenario 3
	Scenario: Login the system with valid credentials
	Given Open browser and start application
	And Provide valid user email and password
	Then User should be logged in
	
	#Scenario 4
	Scenario: Verify presence of My Tasks link
	Given Open browser and start application
	And Provide valid user email and password
	Then My Tasks link is present
	
	#Scenario 5
	Scenario: Verify tasks list after clicking on My Tasks link
	Given Open browser and start application
	And Provide valid user email and password
	When Clicking on My Tasks link
	Then Tasks list is shown
	
	#Scenario 6
	Scenario: Verify message for logged user
	Given Open browser and start application
	And Provide valid user email and password
	When Clicking on My Tasks link
	Then Message for logged user must be Hey USER NAME user, this is your todo list for today:
	
	#Scenario 7
	Scenario: Create a new task by hit Enter key with description correctly filled
	Given Open browser and start application
	And Provide valid user email and password
	When Clicking on My Tasks link
	And Type a valid text in task description field
	And Enter key typed to created task
	Then A new task must be created
	
	#Scenario 8
	Scenario: Create a new task by click add button with description correctly filled
	Given Open browser and start application
	And Provide valid user email and password
	When Clicking on My Tasks link
	And Type a valid text in task description field
	And Click create task button
	Then A new task must be created
	
	#Scenario 9
	Scenario: Create a new task by hit Enter key with description empty
	Given Open browser and start application
	And Provide valid user email and password
	When Clicking on My Tasks link
	When Click on task description input
	And Enter key typed to created task
	Then No task must be created
	
	#Scenario 10
	Scenario: Create a new task by click "+" button with description empty
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link
	When Click create task button
	Then No task must be created
	
	#Scenario 11
	Scenario: Create a new task by hit Enter key with description field with less than 3 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link	
	When Type a text with less than 3 characters in task description field
	And Enter key typed to created task
	Then No task must be created
	
	#Scenario 12
	Scenario: Create a new task by click "+" button with description field with less than 3 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link	
	When Type a text with less than 3 characters in task description field
	And Click create task button
	Then No task must be created
	
	#Scenario 13
	Scenario: Create a new task by hit Enter key with description field with more than 250 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link	
	When Type a text with more than 250 characters in task description field
	And Enter key typed to created task
	Then No task must be created
	
	#Scenario 14
	Scenario: Create a new task by click "+" subtask button button with description field with more than 250 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link	
	When Type a text with more than 250 characters in task description field
	And Click create task button
	Then No task must be created
	
	#Scenario 15
	Scenario: Create a new task by hit Enter key with description field with exactly than 250 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link	
	When Type a text with exactly 250 characters in task description field
	And Enter key typed to created task
	Then A new task must be created
	
	
	#Scenario 16
	Scenario: Create a new task by click "+" button with description field with exactly than 250 characters
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link	
	When Type a text with exactly 250 characters in task description field
	And Click create task button
	Then A new task must be created
	
	#Scenario 17
	Scenario: Verify new task created in tasks list
	Given Open browser and start application
	And Provide valid user email and password
	And Clicking on My Tasks link	
	When Type a valid text in task description field
	And Enter key typed to created task
	Then A new task must be appended to task list